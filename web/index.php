<?php ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- @TODO doplnit descriptiony -->
	    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="author" content="Messenger Messenger">
		<meta name="description" content="">
		<meta property="og:description" content="">
		<meta property="og:title" content="Messenger Messenger">
		<meta name="twitter:description" content="">
		<meta name="twitter:title" content="Messenger Messenger">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta property="og:url" content="http://messages-text.com/">
		<meta http-equiv="content-language" content="en">
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="../images/apple-touch-icon.png" />
		<link rel="apple-touch-icon" sizes="57x57" href="../images/apple-touch-icon-57x57.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-icon-72x72.png" />
		<link rel="apple-touch-icon" sizes="76x76" href="../images/apple-touch-icon-76x76.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-icon-114x114.png" />
		<link rel="apple-touch-icon" sizes="120x120" href="../images/apple-touch-icon-120x120.png" />
		<link rel="apple-touch-icon" sizes="144x144" href="../images/apple-touch-icon-144x144.png" />
		<link rel="apple-touch-icon" sizes="152x152" href="../images/apple-touch-icon-152x152.png" />
		<link rel="apple-touch-icon" sizes="180x180" href="../images/apple-touch-icon-180x180.png" />
		<title>Messenger Messenger</title>
		<link rel="stylesheet" href="../css/screen.css" />
	</head>
	<body id="top">
		<?php
			include 'templates/_navigation.html';
			include 'templates/_banner.html';
			include 'templates/_about.html';
			include 'templates/_zig-zag.html';
			include 'templates/_presskit.html';
			include 'templates/_footer.html';
		?>

		<script src="https://code.jquery.com/jquery.min.js"></script>
		<script src="../js/scroll2Section.js"></script>
		<script src="../js/wow.js"></script>
		<script src="../js/app.js"></script>

	</body>
</html>
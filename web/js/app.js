$(document).ready(function() {
	$(".js-section").scroll2Section({
		menu:'nav',
		activeParent:'li'
	});
	$(".js-burger").on('click', function() {
		$('#burger-check').prop('checked', false);
	});
	wow = new WOW(
		{
            offset: 150,
            mobile: false,
            live: false
        }
    )
    wow.init();
});
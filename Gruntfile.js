module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      dist: {
        options:{
          // style:'compact'
          style:'compressed'
        },
        files: {
          'web/css/screen.css' : 'src/scss/screen.scss'
        }
      }
    },
    autoprefixer:{
      dist:{
        files:{
          'web/css/screen.css':'web/css/screen.css'
        }
      }
    },
    watch: {
      css: {
        files: 'src/scss/**',
        tasks: ['sass', 'autoprefixer']
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.registerTask('default',['watch']);
};
